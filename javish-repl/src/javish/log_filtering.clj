(ns javish.log-filtering
  (:require [clojure.string :as string]))

(defn read-file [file-number]
  (let [base-url "/home/lklich/Pobrane/apache/api.lgi.com_rewrite_www"
        full-url (str base-url file-number)]
    (->> (slurp full-url)
         (string/join)
         (string/split-lines))))

(def apache-logs {:rewrite-www1 (read-file 1)
                  :rewrite-www2 (read-file 2)
                  :rewrite-www3 (read-file 3)
                  :rewrite-www4 (read-file 4)
                  :rewrite-www5 (read-file 5)})


(defn get-only-customer-api [log-lines]
  (->> log-lines
       (filter  #(not (.contains % "sensu-check")))
       (filter #(.contains % "customerApi"))
       (filter #(not (.contains % "integration")))))

(defn only-rewrites [log-lines]
  (filter #(and (.contains % "rewrite")
                (.contains % "->")) log-lines))

(defn only-tunes [log-lines]
  (filter #(.contains % "tune") log-lines))

(defn only-play-vods [log-lines]
  (filter #(.contains % "playVod") log-lines))

(defn only-bookings [log-lines]
  (filter #(.contains % "bookings") log-lines))

(defn from-to-vec [log-lines]
  (map #() log-lines))

(defn from-to-vector [log-lines]
  (map #(drop 1 %)
       (mapcat #(re-seq #"'(.*)' -> '(.*)'" %) log-lines)))

(defn get-rewrites [action-type-filter]
  (->> (apache-logs :rewrite-www2)
       (only-rewrites)
       (action-type-filter)
       (from-to-vector)))

(take 1 (get-rewrites only-tunes))
(take 1 (get-rewrites only-play-vods))
(take 30 (get-rewrites only-bookings))
