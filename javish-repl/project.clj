(defproject javish-repl "0.1.0-SNAPSHOT"
  :description "dummy project just to have all needed jars in place for repl"
  :url "https://github.com/kleewho/javish-repl"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [clj-time "0.9.0"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/algo.monads "0.1.5"]
                 [joda-time "2.8.1"]])
