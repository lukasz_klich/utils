;;; Utils --- utils bla bla
;;; Commentary:
;;; bla bla
;;; Code:

(require 'dash)

;; Austria something.xml failing import
(defun utils-get-transport-id ()
  (save-excursion
    (goto-char (point-at-bol))
    (goto-char  (search-forward "tsid="))
    (forward-char)
    (number-at-point)))

(defun utils-transport-definition-p (line)
  (save-excursion
    (goto-line line)
    (= (- line (line-number-at-pos (search-backward "<TransportStream>"
                                                    (point-min)
                                                    t))) 1)))
(defun utils-list-matched-ids (id)
  (save-excursion
    (goto-char (point-min))
    (utils-list-matched-ids-helper id '())))

(defun utils-list-matched-ids-helper (id acc)
  (let ((next (search-forward
               (concat "<Id>" (number-to-string id) "</Id>")
               (point-max)
               t)))
    (if next (utils-list-matched-ids-helper id (append (list (line-number-at-pos next)) acc))
      acc)))

(defun utils-is-transport-defined-p (id)
  (> (length (-filter 'utils-transport-definition-p (utils-list-matched-ids id))) 0))

(defun utils-check-transport-line ()
  (interactive)
  (print (utils-is-transport-defined-p (utils-get-transport-id))))

(defun utils-list-regionalized-transport-ids ()
  (save-excursion
    (goto-char (point-min))
    (utils-list-regionalized-transport-ids)))

(defun utils-goto-next-regionalized-transport-line ()
  (goto-char (search-forward
              "<RegionalizedTransportStream"
              (point-max)
              t)))

(defun utils-list-regionalized-transport-ids ()
  (goto-char (point-min))
  (remove-duplicates (loop repeat (count-matches "<RegionalizedTransportStream")
                           collect (progn (utils-goto-next-regionalized-transport-line)
                                          (utils-get-transport-id)))))

(defun utils-simple-test ()
  (interactive)
  (print (utils-list-matched-ids 33)))

(defun utils-list-undefined-transports ()
  (interactive)
  (print (save-excursion
           (-reject 'utils-is-transport-defined-p (utils-list-regionalized-transport-ids)))))


(with-current-buffer "something.xml"
  (utils-list-matched-ids 33))
(with-current-buffer "something.xml"
  (goto-line 17)
  (utils-check-transport-line))
(with-current-buffer "something.xml"
  (utils-list-regionalized-transport-ids))
(with-current-buffer "something.xml"
  (utils-list-undefined-transports))


(provide 'utils)
;;; utils.el ends here
