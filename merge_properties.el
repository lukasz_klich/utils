;; -*- lexical-binding: t -*-
(require 's)
(require 'dash)
(require 'subr-x)

(defun read-properties (buffer)
  (set-buffer buffer)
  (->> (buffer-string)
       (s-lines)
       (-filter 'identity)
       (--filter (s-contains? "=" it))
       (--sort (not (s-less-p it other)))
       (--map (-map 's-trim (s-split-up-to "=" it 1)))
       (--reduce-from (puthash-return-hash (elt it 0) (elt it 1) acc)
                      (make-hash-table))))

(defun read-and-sort (buffer)
  (set-buffer buffer)
  (let ((sorted-props (->> (buffer-string)
                           (s-lines)
                           (-filter 'identity)
                           (--sort (s-less-p it other)))))
    (delete-region (point-min) (point-max))
    (insert (mapconcat 'identity sorted-props "\n"))))

(read-and-sort "Properties.merged")

(concat "a" "b")


(s-less? "com.chello.booking.ro = 36000" "com.chello.booking.pl = 36000")

(-sort '(lambda (a1 a2) (not (s-less-p a1 a2))) '("WORootDirectory = /opt/Apple" "com.chello.booking.cache.shouldRetainObjects = false" "com.chello.booking.sendMail = false" "com.chello.booking.traxis.client.maxTimeoutInMillis=3500"))
(defun puthash-return-hash (key value hash-table)
  (puthash (intern key) value hash-table)
  hash-table)

(defun puthash-if-present (key value hash-table)
  (when value
    (puthash key value hash-table)))

(defun diff-two-values (f s)
  (if (string= f s)
      (list nil nil f)
    (list f s nil)))

(defun combine-fn (first second)
  (lexical-let ((f first)
                (s second))
    (lambda (result-tuple key)
      (let ((diff (diff-two-values (gethash key f) (gethash key s))))
        (puthash-if-present key (nth 0 diff) (nth 0 result-tuple))
        (puthash-if-present key (nth 1 diff) (nth 1 result-tuple))
        (puthash-if-present key (nth 2 diff) (nth 2 result-tuple))
        result-tuple))))

(defun diff-hash-tables (first second)
  (->> (hash-table-keys first)
       (-reduce-from (combine-fn first second)
                      (list (make-hash-table) (make-hash-table) (make-hash-table)))))

(write-properties-to-buffer (read-properties (set-buffer "awsProperties")) "testbuffer")

(defun write-it-all-out (first-buffer-name second-buffer-name first-result second-result third-result)
  (let* ((awsProperties (read-properties (set-buffer first-buffer-name)))
         (bhpProperties (read-properties (set-buffer second-buffer-name)))
         (diff (diff-hash-tables awsProperties bhpProperties)))
    (write-properties-to-buffer (elt diff 0) first-result)
    (write-properties-to-buffer (elt diff 1) second-result)
    (write-properties-to-buffer (elt diff 2) third-result)))



(defun write-properties-to-buffer (hash-table buffer-name)
  (with-current-buffer (generate-new-buffer buffer-name)
    (maphash (lambda (key value) (insert (concat (symbol-name key) " = " value "\n"))) hash-table)))

(write-it-all-out "awsProperties" "Properties" "awsProperties.merged" "bhpProperties.merged" "Properties.merged")

(write-it-all-out "awsProperties.wouser" "Properties.wouser" "awsProperties.wouser.merged" "bhpProperties.wouser.merged" "Properties.wouser.merged")


(-each (list "awsProperties.merged" "bhpProperties.merged" "Properties.merged" "awsProperties.wouser.merged" "bhpProperties.wouser.merged" "Properties.wouser.merged") 'kill-buffer)

(kill-buffer "Properties.merged")
