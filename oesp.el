(require 'url)
(require 's)
(require 'request)
;;;;;; oesp

(defun oesp-hours-in-future (h)
  (seconds-to-time (+ (float-time)
                      (* h 3600))))

(defun oesp-format-time (time)
  (format-time-string "%FT%H:%M:%SZ" time))

(defun oesp-listings-endpoint (country locale)
  (concat "https://web-api2.horizon.tv/oesp/api/"
          country "/" locale "/web/listings"))

(defun oesp-hours-in-future-in-ms (h)
  (format-time-string "%s000" (time-add (current-time)
                                     (seconds-to-time (* h 3600)))))

(defun oesp-get-listing (country locale index hours)
  (let ((data (request (oesp-listings-endpoint country locale)
                       :params `(("byStartTime" . ,(concat (oesp-hours-in-future-in-ms hours) "~"))
                                 ("sort" . "startTime")
                                 ("range" . ,(concat index "-" index)))
                       :parser 'json-read-to-hash-table
                       :sync t)))
    (elt data 3)))

(defun oesp-get-imi-and-crid (listings)
  ;; (when (= 0 (length (gethash "listings" listings)))
  ;;   (signal 'empty-listings-error listings))
  (let* ((id (s-split "-" (gethash "id" (elt (gethash "listings" listings) 0))))
         (imi (cadr id))
         (crid (car id)))
    (concat "imi=" (s-replace "imi:" "" imi)
            "&crid=" (s-replace "~~2f" "/" crid))))

(defun oesp-get-random-imi-and-crid (country locale)
  (let ((listings (oesp-get-listing country locale (number-to-string (random 100)) 4)))
    (oesp-get-imi-and-crid listings)))

;;(oesp-get-random-imi-and-crid "PL" "pol")

;;;;;; schedule

(defun schedule-services-endpoint (country)
  (concat "http://lgi.io/kraken/v2/schedule/networks/"
          country "/services.json"))

(defun schedule-get-service (country index)
  (let ((data (request (schedule-services-endpoint country)
                       :params `(("fields" . "feedId")
                                 ("limit" . "1")
                                 ("offset" . ,(number-to-string index)))
                       :parser 'json-read-to-hash-table
                       :sync t)))
    (elt data 3)))

(defun schedule-get-service-id-ref (str)
  (let ((services str))
    (gethash "feedId" (elt (gethash "data" services) 0))))

(defun schedule-get-random-service-id-ref (country)
  (schedule-get-service-id-ref
   (schedule-get-service country (random 99))))
;; (schedule-get-random-service-id-ref "PL")
;;;;;; imi and crid from schedule

(defun schedule-broadcasts-endpoint (country)
  (concat "http://lgi.io/kraken/v2/schedule/data/"
          country "/broadcasts.json"))

(defun schedule-get-broadcast (country index)
  (request (schedule-broadcasts-endpoint country)
           :params `(("fields" . "imi,video.crid")
                     ("limit" . "1")
                     ("start>" . ,(oesp-format-time (oesp-hours-in-future 4)))
                     ("offset" . ,(number-to-string index)))
           :parser 'json-read-to-hash-table
           :sync t))

(defun json-read-to-hash-table ()
  (let ((json-object-type 'hash-table))
    (json-read)))

(defun schedule-get-imi-and-crid (broadcasts)
  (let* ((broadcast (elt (gethash "data" (elt broadcasts 3)) 0))
         (imi (gethash "imi" broadcast))
         (crid (gethash "crid" (gethash "video" broadcast))))
    (concat "imi=" imi "&crid=" crid)))

(defun schedule-get-random-imi-and-crid (country)
  (schedule-get-imi-and-crid (schedule-get-broadcast country (random 100))))

;;(schedule-get-random-imi-and-crid "PL")
